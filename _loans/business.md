---
cid: business
title: Business
abbreviated_tag: Business
slug: business
plan_data:
    status: active
    total_loan: 15000000.32
    prorated: 788500.46
    balance: 13587996.83
    total_paid: 3770417.53
    progress: 45
    tenure:
        period: 24
        duration: Mo.
    interest:
        rate: 15
        per: Interest per mo.
position: 2
---