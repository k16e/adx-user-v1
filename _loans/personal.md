---
cid: personal
title: Personal
abbreviated_tag: Personal
slug: personal
plan_data:
    status: active
    total_loan: 970875.36
    prorated: 70500.46
    balance: 200457.83
    total_paid: 770417.53
    progress: 10
    tenure:
        period: 12
        duration: Mo.
    interest:
        rate: 25
        per: Interest per mo.
position: 1
---