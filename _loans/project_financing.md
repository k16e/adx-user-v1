---
cid: project_financing
title: Project Financing
abbreviated_tag: Project Financing
slug: project-financing
plan_data:
    status: active
    total_loan: 700000.32
    prorated: 88500.46
    balance: 587996.83
    total_paid: 370417.53
    progress: 50
    tenure:
        period: 36
        duration: Mo.
    interest:
        rate: 30
        per: Interest per mo.
position: 4
---