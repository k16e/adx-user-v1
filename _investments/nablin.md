---
cid: nablin
title: Non Asset Backed Lease Investment Note (NABLIN)
abbreviated_tag: NABLIN
slug: nablin
plan_data:
    status: active
    tenure: 36
    funds_invested:
        total_invested: 8768556.89
        current_topup: 93678.76
    returns:
        annual_returns: 9.56%
        total_returns: 75678456.96
        current_returns: 6358875.78
        deferred_returns: 47896.98
        up_or_down: false
        percentage_up_or_down: 2.12%
    withdrawals:
        total_withdrawals: 375364.64
        recent_withdrawal: 2377.34
    management_fee: 1%
    uploads:
        - 1.docx
        - 2.pdf
        - 3.doc
        - 4.csv
        - 5.png
        - 6.jpg
recent_transactions:
    title: Recent Transactions
position: 5
---