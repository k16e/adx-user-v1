---
cid: lin
title: Lease Investment Note (LIN)
abbreviated_tag: LIN
slug: lin
plan_data:
    status: active
    tenure: 12
    funds_invested:
        total_invested: 40768556.89
        current_topup: 3678.76
    returns:
        annual_returns: 6.56%
        total_returns: 5678456.96
        current_returns: 4358875.78
        deferred_returns: 5647896.98
        up_or_down: false
        percentage_up_or_down: 1.32%
    withdrawals:
        total_withdrawals: 75364.64
        recent_withdrawal: 52377.34
    management_fee: 3.2%
    uploads:
        - 1.docx
        - 2.pdf
        - 3.doc
        - 4.csv
        - 5.png
        - 6.jpg
recent_transactions:
    title: Recent Transactions
position: 2
---