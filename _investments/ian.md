---
cid: ian
title: Investment Appreciation Note (IAN)
abbreviated_tag: IAN
slug: ian
plan_data:
    status: active
    tenure: 12
    funds_invested:
        total_invested: 300768556.89
        current_topup: 453678.76
    returns:
        annual_returns: 12.56%
        total_returns: 435678456.96
        current_returns: 24358875.78
        deferred_returns: 5647896.98
        up_or_down: true
        percentage_up_or_down: 0.34%
    withdrawals:
        total_withdrawals: 675364.64
        recent_withdrawal: 452377.34
    management_fee: 1%
    uploads:
        - 1.docx
        - 2.pdf
        - 3.doc
        - 4.csv
        - 5.png
        - 6.jpg
recent_transactions:
    title: Recent Transactions
position: 1
---