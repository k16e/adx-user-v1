---
cid: eip
title: Educational Investment Plan (EIP)
abbreviated_tag: EIP
slug: eip
plan_data:
    status: active
    tenure: 12
    funds_invested:
        total_invested: 7768556.89
        current_topup: 83678.76
    returns:
        annual_returns: 4.56%
        total_returns: 578456.96
        current_returns: 558875.78
        deferred_returns: 67896.98
        up_or_down: true
        percentage_up_or_down: 1.12%
    withdrawals:
        total_withdrawals: 95364.64
        recent_withdrawal: 102377.34
    management_fee: 2%
    uploads:
        - 1.docx
        - 2.pdf
        - 3.doc
        - 4.csv
        - 5.png
        - 6.jpg
recent_transactions:
    title: Recent Transactions
position: 4
---