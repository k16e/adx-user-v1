---
cid: ptp
title: Project Target Plan (PTP)
abbreviated_tag: PTP
slug: ptp
plan_data:
    status: active
    tenure: 24
    funds_invested:
        total_invested: 768556.89
        current_topup: 3678.76
    returns:
        annual_returns: 8.56%
        total_returns: 678456.96
        current_returns: 358875.78
        deferred_returns: 47896.98
        up_or_down: true
        percentage_up_or_down: 4.12%
    withdrawals:
        total_withdrawals: 475364.64
        recent_withdrawal: 8377.34
    management_fee: 2%
    uploads:
        - 1.docx
        - 2.pdf
        - 3.doc
        - 4.csv
        - 5.png
        - 6.jpg
recent_transactions:
    title: Recent Transactions
position: 3
---