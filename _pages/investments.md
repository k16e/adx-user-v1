---
layout: investments
title: Investments
permalink: /investments/
type_details:
    cumulative_totals: 138976570.99
    accompanying_text: Total balances
add_new:
    show: true
    text: Invest
    link: /invest/
investment_data:
    total_money_invested: 15000000.32
    current_earned: 788500.46
    lifetime_earned: 13587996.83
current_accounts:
    title: Current Investments
recent_transactions:
    title: Recent Transactions
---