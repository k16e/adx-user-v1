---
layout: loans
title: Loans
permalink: /loans/
type_details:
    cumulative_totals: 138976570.99
    accompanying_text: Total balances
add_new:
    show: true
    text: New Loan
    link: /new-loan/
current_accounts:
    title: Current Loans
recent_transactions:
    title: Recent Transactions
accounts_pie:
    title: Current Loans
---