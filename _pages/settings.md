---
layout: settings
title: Profile & Settings
permalink: /settings/
settings_grouping:
    - personal
    - account
    - business
    - next of kin
---