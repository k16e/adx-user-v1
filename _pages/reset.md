---
layout: reset
title: Reset Password
description: Enter and confirm your new password and you'll be all done.
on_submit:
    success_message: Password reset successful.
    button_text: Log in
permalink: /reset/
image:
    image: /images/woman-sitting-in-front-of-a-computer-while-talking-on-the-phone.jpg
    description: Woman sitting in front of a computer while talking on the phone
---